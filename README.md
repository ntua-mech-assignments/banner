# Banner
The purpose of this project is to read a text in "greeklish" and display greek letters formed by special characters (e.g. *,#,$).
The result can either be written to a file or printed to the screen, not both.   
The program takes inputs from the command line using argc/argv, with the arguments being the following :  
* The text to be displayed.
* A path to a file containing the character library (characters formed with symbols).
The first line of this file contains the dimensions of the characters. 
* (optional) A path to a file where the results will be printed.