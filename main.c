#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h> 

# define MAX_LINE_LENGTH 100 // this is actually only used for the fontfile letter dimensions line
 

void fontfile_structure( FILE* fontfile, int* dims); 
void print_letter_line( FILE* fontfile, FILE* outfile, int* dims, int letter_pos, bool is_space, bool tofile); 
int letter_position(int* dims, char letter); 
void print_message( FILE* fontfile, FILE* outfile, int* dims, char input_string[], int input_len, bool tofile);


int main(int argc, char *argv[]){

    // Receive input text and place it into a string 
    int input_len = strlen(argv[1]);
    char input_text[input_len];
    strcpy(input_text,argv[1]); 
    
    // Check for the optional output file argument 
    bool tofile = false ; 
    int outfilename_len = 0 ; 
    char outfilename[FILENAME_MAX];
    FILE* outfile = NULL ; 

    if(argc == 4){
        outfilename_len = strlen(argv[3]);
        strcpy(outfilename,argv[3]); 
        tofile = true ; 

        outfile = fopen(outfilename,"w"); 
        if (outfile == NULL){
            printf("Error opening output file\n"); 
        return -1 ; 
        }
    }

    // Determine the path of the fontfile 
    int fontfilename_len = strlen(argv[2]); 
    char fontfilename[fontfilename_len];
    strcpy(fontfilename,argv[2]); 

    // Create font file pointer 
    FILE* fontfile ; 
    fontfile = fopen(fontfilename,"r"); 
    if (fontfile == NULL) {
        printf("Error opening font file\n");
        return  -1; 
    }
 
    // Determine the structure of the letters
    int dims[] = {0,0}; 
    fontfile_structure(fontfile,dims) ; 
    //printf("Width = %d, Height = %d \n",dims[0],dims[1]); 

    print_message(fontfile,outfile,dims,input_text,input_len,tofile);

    fclose(fontfile); 
    fclose(outfile); 

    return 0 ; 
}

void fontfile_structure(FILE* fontfile, int* dims){    
    int height = 0 ; 
    int width = 0 ; 
    char dimension_line[MAX_LINE_LENGTH];
    fgets(dimension_line,MAX_LINE_LENGTH,fontfile); 
    sscanf(dimension_line,"%d %d",&dims[0],&dims[1]); 
    fseek(fontfile, 0, SEEK_SET);
    // rewind(fontfile); //re-set cursor to the beggining of the files
}

void print_letter_line(FILE* fontfile, FILE* outfile, int* dims,int letter_pos, bool is_space, bool tofile){
    // The height argument denotes which line of the letter we are printing
    // The letter_pos argument denotes the line of the start of the letter
    
    char letter_line[MAX_LINE_LENGTH] ; 
    if (is_space==true){
        if(tofile == true){
            fprintf(outfile,"  ");
        }
        else{
            printf("  "); 
        }
    }
    else {

	    for (int i = 0 ; i <= letter_pos ; i ++ ){
		fgets(letter_line,MAX_LINE_LENGTH,fontfile); 
		// printf("%d",i);
		if (i < letter_pos){
		    continue ; 
		}

		// due to a malformed fontfile, some letters might contain whitespaces after the symbols.
		letter_line[dims[0]] = '\0'; 
	  
		// Print to output file, if requested 
		if(tofile == true ){
		    fprintf(outfile,"%s ",letter_line); 
		}
		else{
		    // print the letter line and a space to differentiate letters
		    printf("%s ",letter_line);   
		}
	    }
    }
    fseek(fontfile, 0, SEEK_SET); // re-wind file pointer to the begining
}

void print_message(FILE* fontfile, FILE* outfile, int* dims, char input_string[], int input_len,bool tofile){
    int letter_row = 0 ; 
    char letter ; 

    // count until one less element from the end of the string to avoid reading the '\0'
    for(int j = 0 ; j < dims[1]; j ++){
        for(int i = 0 ; i < input_len  ; i ++){
            letter = input_string[i];
            bool is_space = false;
            if (letter==32) {
            	is_space=true;
            }
            letter_row = letter_position(dims,letter); 
            print_letter_line(fontfile,outfile,dims,letter_row+j-1,is_space,tofile); 
        }

        // Print to outputfile, if needed 
        if (tofile == true){
            fprintf(outfile,"\n"); 
        }
        else{
            printf("\n");
        }

    }

}

int letter_position(int * dims, char letter){

char uppercase_symbols_seq[] = {'A', 'B', 'G', 'D', 'E', 'Z', 'H', 'U', 'I', 'K', 'L', 'M', 'N', 'J', 'O', 'P', 'R', 'S', 'T', 'Y', 'F', 'X', 'C', 'V', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '.', '!'};

char lowercase_symbols_seq[] = {'a', 'b', 'g', 'd', 'e', 'z', 'h', 'u', 'i', 'k', 'l', 'm', 'n', 'j', 'o', 'p', 'r', 's', 't', 'y', 'f', 'x', 'c', 'v'};

int uppercase_symbols_seq_Len = sizeof uppercase_symbols_seq / sizeof uppercase_symbols_seq[0];
int lowercase_symbols_seq_Len = sizeof lowercase_symbols_seq / sizeof lowercase_symbols_seq[0];
int index = -1;    
     for (int i= 0; i < uppercase_symbols_seq_Len; i++) {
        if (uppercase_symbols_seq[i] == letter) {
            index = i;
            break;
         }
    }
     for (int j = 0; j < lowercase_symbols_seq_Len; j++) {
       if (lowercase_symbols_seq[j] == letter) {
            index = j;
            break;
         }
    }    
    
    if (index > -1) {

    } else {
         index = uppercase_symbols_seq_Len-1;
    }
     

    int letter_offset = 3; // to account for the dimension line and the first separator line
    return letter_offset + (dims[1]+1)*index;
    
}

